

  Feature: LoginFeature
    feature that test multiple login process test case


    Scenario: login with correct username and password with DataTable
      Given I navigate to the login page
        | urladress   | urlname   |
        | urladress01 | urlname01 |
        | urladress02 | urlname02 |
        | urladress03 | urlname03 |
        | urladress04 | urlname04 |
        | urladress05 | urlname05 |
        | urladress06 | urlname06 |
      And I enter the correct "username" and the correct "password"
      And i click the login button
      Then I check the userform page

    Scenario Outline: login with correct username and password with outline scenario
      Given I navigate to the adress
      And I navigate to the adress <urladress> with the name <urlname> correctly
      And I enter the correct "username" and the correct "password"
      And i click the login button
      Then I check the userform page

    Examples:
        | urladress   | urlname   |
        | urladress01 | urlname01 |
        | urladress02 | urlname02 |
        | urladress03 | urlname03 |
        | urladress04 | urlname04 |
        | urladress05 | urlname05 |
        | urladress06 | urlname06 |
