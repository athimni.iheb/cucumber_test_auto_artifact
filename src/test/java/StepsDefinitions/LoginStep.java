package StepsDefinitions;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LoginStep {
    @Given("I navigate to the login page")
    public void iNavigateToTheLoginPage(DataTable table) throws Throwable {
        System.out.println("I navigate to the login page");
        List<List<String>> data = table.asLists(String.class);
//        System.out.println("the first column first value is : "+ table.cell(0,0));
//        System.out.println("the first column second value is : "+ table.cell(0,1));
        int rowNumber = table.height();
        for (int i= 1; i< rowNumber; i++)
        {
            System.out.println("the url Adress   number "+ i + " is : " + table.cell(i,0));
            System.out.println("the Url Name     number "+ i + " is : " + table.cell(i,1));
        }
    }


    @Given("I navigate to the adress")
    public void iNavigateToTheAdress() {
        System.out.println("I navigate to the adress");
    }

    @And("I enter the correct {string} and the correct {string}")
    public void iEnterTheCorrectAndTheCorrect(String username, String password) {
        System.out.println("UserName is " + username);
        System.out.println("Password is " + password);
        System.out.println("I enter the correct username and the correct password");
    }

    @And("i click the login button")
    public void iClickTheLoginButton() {
        System.out.println("i click the login button");
    }

    @And("I navigate to the adress <urladress> with the name <urlname> correctly")
    public void iNavigateToTheAdressUrladressWithTheNameUrlnameCorrectly() {
    }

    @Then("I check the userform page")
    public void iCheckTheUserformPage() {
        System.out.println("I check the userform page");
    }

}
